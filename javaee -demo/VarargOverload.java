/* 
Requirement:
   - demonstrate overloading with varArgs
Entity:
   - VarargOverload
Function Declaration:
   - private void test(int ... args)
   - private void test(boolean p, String ... args)
   - public static void main( String[] args )    
Jobs to be done:
   1.Declare the test() method with parameter used in integer datatype .
   2.A variable-length argument is specified by three periods(�).
   3.Initialize the variable sum is declared with datatype integer.
   4.If the condition is true then print the sum value
   5.Initialize the variable negate is declared with datatype boolean
   6.Then print the negate value
   7.Display the obj.test()
*/
class VarargOverload {

    private void test(int ... args){
        int sum = 0;
        for (int i: args) {
            sum += i;
        }
        System.out.println("sum = " + sum);
    }

    private void test(boolean p, String ... args){
        boolean negate = !p;
        System.out.println("negate = " + negate);
        System.out.println("args.length = "+ args.length);
    }

    public static void main( String[] args ) {
        VarargOverload obj = new VarargOverload();
        obj.test(1, 2, 3);
        obj.test(true, "hello", "world");
    }
}

