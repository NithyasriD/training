/*Requirements:
 Change the following program to use compound assignments:
       class ArithmeticDemo {

            public static void main (String[] args){

                 int result = 1 + 2; // result is now 3
                 System.out.println(result);

                 result = result - 1; // result is now 2
                 System.out.println(result);

                 result = result * 2; // result is now 4
                 System.out.println(result);

                 result = result / 2; // result is now 2
                 System.out.println(result);

                 result = result + 8; // result is now 10
                 result = result % 7; // result is now 3
                 System.out.println(result);
            }
       }
Entity : 
    -ArithmeticDemo
Function Declaration:
    -public static void main (String[] args)
Jobs to be done:
    1.Declare and initialize the number of type integer with value 3.
    2.Decrement the number by 1 and store it in number. 
    3.Multiply the number by 2 and store it in number.
    4.Divide the number by 2 and store it in number.
    5.Add the number by 8 and store it in number.
    6.Remainder by 7 and store it in number.
    7.Display the number.	
 */

public class ArithmeticDemo {

	public static void main(String[] args) {
		
		 int number = 3;  //result is now 3
	     System.out.println(number);

	     number -= 1; // result is now 2
	     System.out.println(number);

	     number *= 2; // result is now 4
	     System.out.println(number);

	     number /= 2; // result is now 2
	     System.out.println(number);

	     number += 8; // result is now 10
	     number %= 7; // result is now 3
	     System.out.println(number);	        
	}

}
