/*
    Requirements : To demonstrate inheritance,overloading,overriding using Animal,Dog,Cat,Snake classes.

    Entities : public class Animal
                   public class Dog
                   public class Cat
	   public class Snake
			   
    Function Declaration : public void action(String name, String color)
		        public void bark()
		        public void action()
		        public static void main(String[] args)

    Jobs to be done :
                      1. Inside class Animal variables name and color are declared.
                      2. A method action() is defined with two parameters.(Method Overloading)
                      3. name and color is displayed inside this function.
                      4. Inside main function an object animal is created for Animal class.
                      5. The two methods are called with suitable arguments.
                      6. A class Dog is created which extends the class Animal.(Inheritance)
                      7. Inside class Dog a method bark() is created.
                      8. A method action() is created in class Dog with no parameters.
                      9. Inside the main function of class Dog an object dog is created.
                     10. The method action() is called using object dog.(Method Overriding)
                     11. A class Cat is created which extends the class Animal.(Inheritance)
           	     12. Inside class Cat, a method action() is created.
	     13. A class Snake is created which extends the class Animal.(Inheritance)
	     14. Inside class Snake a method action() is created.
*/  					  


public class Main {
      public static void main(String[] args) {
             Cat cat = new Cat();
             Dog dog = new Dog();
             Snake snake = new Snake();
             Animal animal = new Animal();
		
             cat.action();
             cat.action("Cat", "Brown");
             dog.bark();
             dog.action();
             dog.action("Dog", "White");
             snake.action();
             snake.action("Snake", "Green");
             animal.action("Elephant", "Black");
      }
}











