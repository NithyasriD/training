/* 
Requirement:
print the type of the result value of following expressions
  - 100 / 24
  - 100.10 / 10
  - 'Z' / 2
  - 10.5 / 0.5
  - 12.4 % 5.5
  - 100 % 56
Entity:
   -ReturnDatatype
Function Declaration:
   -public static void main(String[] args)
Jobs to be done:
   1. Declare the object values.
   2. Divide the number 100 by 24 and result should placed in a
   3. Divide the number 100.10 by 10 and result should placed in b  
   4. Divide the value z by 2 and result should placed in c
   5. Divide the number 10.5 by 0.5 and result should placed in d
   6. Get the remainder value of 12.4 by 5.5 and result should placed in e
   7. Get the remainder value of 100 by 56 and result should placed in f
   8. Display the getName() of the Objects
*/
 
 public class ReturnDatatype {
		
      public static void main(String[] args) { 
		
            Object a = 100 / 24 ;
            Object b = 100.10 / 10;
            Object c = 'Z' / 2 ;
            Object d = 10.5 / 0.5 ;
            Object e = 12.4 % 5.5;
            Object f = 100 % 56;
			
			System.out.println(a);
            System.out.println(b);
            System.out.println(c);
            System.out.println(d);
            System.out.println(e);
            System.out.println(f);
			
            System.out.println(a.getClass().getName());
            System.out.println(b.getClass().getName());
            System.out.println(c.getClass().getName());
            System.out.println(d.getClass().getName());
            System.out.println(e.getClass().getName());
            System.out.println(f.getClass().getName());
 
        }
 }
	
	
