/*
Requirement:
  - print fibonacci using for loop, while loop and recursion
Entity:
  - FibonacciForExample
  - FibonacciWhileExample
  - FibonacciRecursionExample
Function Declaration:
  - public static void main(String[] args)  
Jobs to be done:
  1. Understand the concept of fibonacci series.
  2. Get the number from the user
  3. Using the for,while,recursion and print the previousNumber and nextNumber
  4. Add two numbers into a variable called sum
  5. Print the number
  6. Then assign the values of nextNumber in previousNumber and nextNumber as sum
  7. Repeat the loop until it satisfies the condition in for loop ,while loop ,recursion.
*/

  
  
//for loop

public class FibonacciForExample {
 
	public static void main(String[] args) 
	{
		 int maxNumber = 10; 
		 int previousNumber = 0;
		 int nextNumber = 1;
		 
	        System.out.println("Fibonacci Series : ");
 
	        for (int i = 1; i <= maxNumber; ++i)
	        {
	            System.out.println(previousNumber+" ");
	            int sum = previousNumber + nextNumber;
	            previousNumber = nextNumber;
	            nextNumber = sum;
	        }		
	}
 
}








