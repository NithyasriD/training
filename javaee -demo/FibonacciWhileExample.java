//while loop

public class FibonacciWhileExample {
 
	public static void main(String[] args) 
	{
		 int maxNumber = 10, previousNumber = 0, nextNumber = 1;
		 
	        System.out.println("Fibonacci Series : ");
	        
			int i=1;
	        while(i <= maxNumber)
	        {
	            System.out.println(previousNumber+" ");
	            int sum = previousNumber + nextNumber;
	            previousNumber = nextNumber;
	            nextNumber = sum;
	            i++;
	        }
   	 }
 
}




