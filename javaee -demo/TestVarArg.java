/* 
Requirement:
   - demonstrate overloading with varArgs
Entity:
   - TestVarArg
Function Declaration:
   - static void methodOne(int ... a)     
   - static void methodOne(boolean ... a) 
   - static void methodOne(String str, int ... a) 
   - public static void main(String args[]) 
Jobs to be done:
   1.Declare the methodOne() with parameter used in integer datatype .
   2.A variable-length argument is specified by three periods(�).
   3.Initialize the variable x is declared with datatype integer.
   4.If the condition is true then print the x value
   5.Initialize the variable x is declared with datatype boolean
   6.Then print the x with boolean value.
   7.Initialize the variable x is declared with datatype String.
   8.Then print the x with String value 
   9.Display the methodOne()
*/



/*
Overloading allows different methods to have same name, but different signatures where signature can differ 
by number of input parameters or type of input parameters or both.
*/


class TestVarArg 
{ 
	static void methodOne(int ... a) 
	{ 
		System.out.print("Number of args: " + a.length + " Contents: "); 	
		 
		for(int x : a) 
			System.out.print(x + " "); 
		
		System.out.println(); 
	} 
	 
	static void methodOne(boolean ... a) 
	{ 
		System.out.print("Number of args: " + a.length + " Contents: "); 	
		
		for(boolean x : a) 
			System.out.print(x + " "); 
		
		System.out.println(); 
	} 
	
	static void methodOne(String str, int ... a) 
	{ 
		
		System.out.print(str + a.length + " Contents: "); 	
		
		for(int x : a) 
			System.out.print(x + " "); 
		
		System.out.println(); 
	} 
	
	public static void main(String args[]) 
	{ 
		
		methodOne(1, 2, 3); 
		methodOne("Testing: ", 10, 20); 
		methodOne(true, false, false); 
	} 
} 
