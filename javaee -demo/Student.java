public class Student  {

    public String name;
	public String address;
	public int rollnumber;
	
	//creating constructor
	public Student(String name, String address, int rollnumber) {
	    this.name = name;
		this.address = address;
        this.rollnumber = rollnumber;
	}
    
	//private constructor
    private Student(String name, String address) {
        System.out.println("Student Name: "+name+"\nStudent Address :"+address+"\n");
    }

    //non parameterized constructor
    public Student() {
        System.out.println("Student Address :"+address+"\n");
    }
   
    //Method Definition
    protected void printStudentDetails() {
        System.out.println("\nStudent Name :"+name+"\nStudent Address :"+address+"\nStudent rollnumber :"+rollnumber+"\n");
    }
  
    //Object Initializer
    {
        address = "Tiruppur";
    }
     
	//Main Function 
    public static void main(String[] args) {  
        Student stud = new Student("Nithya", "Coimbatore", 18093);	
	    stud.printStudentDetails();
		College college = new College();
		college.run();                      
		Professor professor = new Professor();
		professor.run();                    
	}
}	
		 
		 
		 
		 
		 
		 