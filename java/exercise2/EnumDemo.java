/*
Requirement:
  - compare the enum values using equal method and == operator
Entity:
  - EnumDemo
Function Declaration:
  - public static void main(String[] args) 
Jobs to be done:
   1.Consider the program
   2.enum methods have been compared
   3.Then it throws an exception NullPointerException()
   4.Display the value   
*/

/*
enum:  

  * In enum is defined as a collections of constants.
  * An enum can contain constants, methods .
  * enum can be defined as a group of named constant.
  * enum values using equal() method and == operator. 
  * equal(=) means assigned the value,== means compared the value 
   
*/
  
  
/* 
Difference:
 
   == operator never throws NullPointerException whereas equals() method can throw NullPointerException.
   == is responsible for type compatibility check at compile time whereas equals() method will never worry about the types of both the arguments.

*/   
 

 
class EnumDemo { 
    public enum Day { MON, 
                      TUE, 
                      WED, 
                      THU, 
                      FRI, 
                      SAT, 
                      SUN } 
      public static void main(String[] args) 
      { 
	  try {
		  
         Day day = null;
         System.out.println(day == Day.MON);
         System.out.println(day.equals(Day.MON)); 
		 throw new NullPointerException("default");
	     } catch (Exception e){
			 System.out.println("divide by zero");
		 } finally{
			 
		 }	 
	  }  
}    
