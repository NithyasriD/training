/*
Requirement:
  - Create a program that is similar to the previous one but instead of reading integer arguments, it reads floating-point arguments.
    It displays the sum of the arguments, using exactly two digits to the right of the decimal point.
    For example, suppose that you enter the following: java FPAdder 1 1e2 3.0 4.754
    The program would display 108.75. Depending on your locale, the decimal point might be a comma (,) instead of a period (.).
Entity:
  - FPAdder
Function Declaration:
  - public static void main(String[] args)
Jobs to be done:
  1.Declare the numArgs of type integer
  2.Check the condition numArgs < 2 and print the text line
  3.Above condition is false,then go to the else 
  4.Display the output  
*/
    
	
import java.text.DecimalFormat;

public class FPAdder {
    public static void main(String[] args) {

	  int numArgs = args.length;
       if (numArgs < 2) {
           System.out.println("This program requires two command-line arguments.");
       } else { 
	    double sum = 0.0;
	    for (int i = 0; i < numArgs; i++) {
		
                sum += Double.valueOf(args[i]).doubleValue();
	    }
	    DecimalFormat myFormatter = new DecimalFormat();
	    String output = myFormatter.format(sum);
            System.out.println(output);
        }
    }
}	


