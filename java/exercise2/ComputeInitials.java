/*
Requirement:
 - Write a program that computes your initials from your full name and displays them.
Entity:
 - ComputeInitials
Function Declaration:
 - public static void main(String[] args) 
Jobs to be done:
  1. Declare the name by type String
  2. Get the name length of type integer
  3. Check the condition i < length and increment the value  
  4. Check the character is UpperCase then Get the result in myInitials based on index value
  5. Display myInitials.  
*/

public class ComputeInitials {
    public static void main(String[] args) {
        String myName = "D.Nithya Sri";
        StringBuffer myInitials = new StringBuffer();
        int length = myName.length();

        for (int i = 0; i < length; i++) {
            if (Character.isUpperCase(myName.charAt(i))) {
                myInitials.append(myName.charAt(i));
            }
        }
        System.out.println("My initials are: " + myInitials);
    }
}

