/* Requirements:
+   Show two ways to concatenate the following two strings together to get the string "Hi, mom.":
       String hi = "Hi, ";
       String mom = "mom.";
Entity:
    -ConcatenateDemo
Function Declaration:
	-public static void main(String[] args)
Jobs to be done:
    1. Declare the name "hi" and "mom" of type String 
    2. Two String names should be concatenated
    3. Display the Result
*/	
	
/*	
Answer:

    hi.concat(mom) 
	hi + mom.	

*/ 
	
public class ConcatenateDemo {
    public static void main(String[] args) {
      String hi = "Hi, ";
      String mom = "mom.";
      String Result = "hi + mom." ; 
	  String ResultEx = hi.concat(mom) ;
	  
	  System.out.println(Result);
	  System.out.println(ResultEx);
	  
    }
}	  
	  