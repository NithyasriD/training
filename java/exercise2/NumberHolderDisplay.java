/*
Requirement:
  - Given the following class, called NumberHolder,
    write some code that creates an instance of the class and initializes its two member variables with provided values,
    and then displays the value of each member variable.

    public class NumberHolder {
        public int anInt;
        public float aFloat;
    }
Entity:
  - NumberHolderDisplay
Function Declaration:
  - public static void main(String[] args) 
Jobs to be done:
  1. Declare and Initialize the number of type integer with value 5
  2. Declare and Initialize the number of type float with value 4.4
  3. Disply integer value
  4. Disply float value
*/  


 public class NumberHolderDisplay {
    public static void main(String[] args) {
	   NumberHolder num = new NumberHolder();
	   num.anInt = 5;
	   num.aFloat = 4.4;
	   System.out.println(num.anInt);
	   System.out.println(num.aFloat);
    }
 }
 
 
 