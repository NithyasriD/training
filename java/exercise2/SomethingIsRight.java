/*Requirement:
  What's wrong with the following program? And fix it.

    public class SomethingIsWrong {
        public static void main(String[] args) {
            Rectangle myRect;
            myRect.width = 40;
            myRect.height = 50;
            System.out.println("myRect's area is " + myRect.area());
        }
    }
Entity :
    -SomethingIsWrong(Given)
Function Declaration :
    -public static void main(String[] args)
Jobs to be done :
    1.Create an object myRect.
    2.The output will be executed without any error.	
    3.Display the myRect area.
*/	


/*Answer:

   This code not create a Rectangle object.
   compilation error should be occured.
   However, myRect might be initialized to null in one place, say in a constructor. 
   In that case,the program will compile, but will generate a NullPointerException at runtime.
 
 */ 
  
  

public class SomethingIsRight {
    public static void main(String[] args) {
        Rectangle myRect = new Rectangle();
        myRect.width = 40;
        myRect.height = 50;
        System.out.println("myRect's area is " + myRect.area());
    }
}
   
   
   
