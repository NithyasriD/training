/*
Requirement:
  - Create a program that reads an unspecified number of integer arguments from the command line and adds them together.
    For example, suppose that you enter the following: java Adder 1 3 2 10
    The program should display 16 and then exit. The program should display an error message if the user enters only one argument.
Entity:
  - Adder
Function Declaration:
  - public static void main(String[] args)
Jobs to be done:
  1.Declare the numArgs of type integer
  2.Check the condition numArgs < 2 and print the text line
  3.Above condition is false,then go to the else 
  4.Display the sum  
*/
   	


public class Adder {
    public static void main(String[] args) {
	int numArgs = args.length;
        if (numArgs < 2) {
            System.out.println("This program requires two command-line arguments.");
        } 
		else 
		{  
	    int sum = 0;
	    for (int i = 0; i < numArgs; i++) {
                sum += Integer.valueOf(args[i]).intValue();
	    }
            System.out.println(sum);
        }
    }
}	




