/* 
Requirements:
    -In the following program, called ComputeResult, what is the value of result after each numbered line executes?
Entity:
    -ComputeResult
Function Declaration:
    - public static void main(String[] args)
Jobs to be done:
    1. Declare the name "software" of type String
    2. Get the index value[0] from software and print the result
    3. Decrement the number by 1 in original length and print the result
    4. Get the result in 1 and index value[4] should be insert
    5. Get the result in 1 and substring(1,4) should be append
    6. Get the result in 3 and substring(5,7) should be append
    7. Display the result
*/
 	
/*	
Answer:

1.si
2.se
3.swe
4.sweoft
5.swear oft	

*/
 

public class ComputeResult {
        public static void main(String[] args) {
            String original = "software";
            StringBuilder result = new StringBuilder("hi");
            int index = original.indexOf('a');

    /*1*/   result.setCharAt(0, original.charAt(0));
	        System.out.println(result);
			
    /*2*/   result.setCharAt(1, original.charAt(original.length()-1));
	        System.out.println(result);
			
    /*3*/   result.insert(1, original.charAt(4));
	        System.out.println(result);
			
    /*4*/   result.append(original.substring(1,4));
	        System.out.println(result);
			
    /*5*/   result.insert(3, (original.substring(index, index+2) + " "));
            System.out.println(result);
        }
    }