package students;

import programs.*;

public class Student  {

    public String name;
	public String address;
	public int rollnumber;
	
	//creating constuctor
	public Student(String name, String address, int rollnumber) {
	    this.name = name;
		this.address = address;
        this.rollnumber = rollnumber;
	}
    
	//private constuctor
    private Student(String name, String address) {
        System.out.println("Student Name: "+name+"\nStudent Address :"+address+"\n");
    }

    //non parameterised constuctor
    public Student() {
        System.out.println("Student Address :"+address+"\n");
    }
   
    //Method Definition
    protected void printStudentDetails() {
        System.out.println("\nStudent Name :"+name+"\nStudent Address :"+address+"\nStudent rollnumber :"+rollnumber+"\n");
    }
  
    //Object Initializer
    {
        address = "Tiruppur";
    }
     
	 
	//Main Function 
    public static void main(String[] args) {
    
         Student stud = new Student("Nithya", "Coimbatore", 18093);	
		 stud.printStudentDetails();
		 Student stud1 = new Student("Sri", "Chennai");	
		 stud1.printStudentDetails();
		 Student stud2 = new Student();
		 Student stud3 = new Student("Nathiya", "Erode");
		 College college = new College();
		 college.run1();                      //Compile Time Error:Private Access in College
		 Professor professor = new Professor();
		 professor.run();                     //Compile Time Error:Protected Access in Professor
		 stud.run();
		 
	}
}	
		 
		 
		 
		 
		 
		 