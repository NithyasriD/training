
public class Dog {
     
	public String breed;
	public String name;
	 
	public Dog(String breed,String name) {
        breed = breed;
        name = name;
    }
    private Dog(String breed) {
        breed = breed;
    }
    
	public Dog() {
	
	} 
	
    {
        name = "Jack";
    }

    public void printInfo() {
        System.out.println("Breed:"+ breed + "\nName:" + name+"\n");
    }
    
    public static void main(String[] args) {

        Dog d1 = new Dog();
        Dog d2 = new Dog("pug");
		d2.breed = "pug";
        d1.printInfo();
		d2.printInfo();
    }
}

