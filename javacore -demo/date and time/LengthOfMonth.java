/*Requirement: To Find the Length of the give year?.
 * 
 * Entity: LengthOfMonth
 * 
 * Function Declaration: public static void main(String[] args);
 * 
 * Jobs to be done:
 * 		1. Assign the input year to year variable. 
 * 		2.For each month in the year.
 * 			2.1) Find the number of days 
 * 			2.2) Print the month of the year and days of month.
 * 
 * PseudoCode:

public class LengthOfMonth {
	
	public static void main(String[] args) {
        int year = 2020;
        
        System.out.printf("For the year %d:%n", year);
        for (Month month : Month.values()) {
            YearMonth ym = YearMonth.of(year, month);
            System.out.printf("%s: %d days%n", month, ym.lengthOfMonth());
        }
    }
}

 * */
package com.training.java.core.date;

import java.time.Month;
import java.time.YearMonth;

public class LengthOfMonth {
	
	public static void main(String[] args) {
		
        int year = 2020;  
        System.out.printf("For the year %d:%n", year);
        for (Month month : Month.values()) {
            YearMonth ym = YearMonth.of(year, month);
            System.out.printf("%s: %d days%n", month, ym.lengthOfMonth());
        }
        
    }
	
}