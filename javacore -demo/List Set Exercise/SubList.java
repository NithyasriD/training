package com.training.java.core.list;
/*Requirements:
   Sublist with example.
   
Entity:
   SubList 
    
Function Declaration:
   public static void main(String[] args)
   
Jobs to be done:
 1.Initializing a list of type LinkedList.
 2.Add five list elements in add() method.
 3.Create a new list of type Integer
 4.Print the subList values in sub
  
Pseudo Code:
public class SubList { 	
    public static void main(String[] args) {
        // Initializing a list of type LinkedList 
        List<Integer> l = new LinkedList<>(); 
        
        l.add(102); 
        l.add(303); 
        l.add(504); 
        l.add(707); 
        l.add(301);
        l.add(908);
        l.add(402); 
        
        List<Integer> sub = new LinkedList<>(); 
        
        System.out.println(l); 
        sub = l.subList(2, 5); 
        System.out.println(sub);
    }
}

*/

import java.util.*; 

public class SubList { 
	
    public static void main(String[] args) {
    	
        // Initializing a list of type LinkedList 
        List<Integer> l = new LinkedList<>(); 
        
        l.add(102); 
        l.add(303); 
        l.add(504); 
        l.add(707); 
        l.add(301);
        l.add(908);
        l.add(402); 
        
        List<Integer> sub = new LinkedList<>(); 
        
        System.out.println(l); 
        sub = l.subList(2, 5); 
        System.out.println(sub);
        
    }
    
}


