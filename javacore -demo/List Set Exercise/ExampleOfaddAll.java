package com.training.java.core.list;
/*Requirements:
   Create another list and perform addAll() method with it.
   
Entity:
   ExampleOfaddAll
    
Function Declaration:
   public static void main(String[] args)
   
Jobs to be done:
 1.Creating a new array list with type string .
 2.Add the values to the list using add() method.
 3.Creating an another list and adding the values to the list.
 4.Adding all the elements of a list to the another list.
 5.Display the list.
 
Pseudo Code:
public class ExampleOfaddAll {
    public static void main(String[] args) { 
        // ArrayList1 of String type
        ArrayList<String> al = new ArrayList<String>();
        al.add("Hi");
        al.add("welcome");
        al.add("java");
        al.add("class");
        System.out.println("ArrayList1 before addAll:"+al);
        
        //ArrayList2 of String Type
        ArrayList<String> al2 = new ArrayList<String>();
        al2.add("Have");
        al2.add("fun");
        al2.add("with");
        al2.add("java");
        
        //Adding ArrayList2 into ArrayList1
        al.addAll(al2);
        System.out.println("ArrayList1 after addAll:"+al);
    }
} 
*/

 
import java.util.ArrayList;

public class ExampleOfaddAll {
	
    public static void main(String[] args) { 
    	
        // ArrayList1 of String type
        ArrayList<String> al = new ArrayList<String>();
        al.add("Hi");
        al.add("welcome");
        al.add("java");
        al.add("class");
        System.out.println("ArrayList1 before addAll:"+al);
        
        //ArrayList2 of String Type
        ArrayList<String> al2 = new ArrayList<String>();
        al2.add("Have");
        al2.add("fun");
        al2.add("with");
        al2.add("java");
        
        //Adding ArrayList2 into ArrayList1
        al.addAll(al2);
        System.out.println("ArrayList1 after addAll:"+al);
        
    }
    
}
