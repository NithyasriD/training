package com.training.java.core.list;
/*Requirement:
   Find the index of some value with indexOf()
   
Entity:
   IndexOf 
   
Function Declaration:
   public static void main(String[] args)
   
Jobs to be done:
 1.Creating a new array list with type string.
 2.Adding the values to list using add() method.
 3.Print the values 
   3.1)Print the index of a particular elements in a find list.
   3.2)Print -1 if the specified element is not present in the list.
 5.Display the value.
 
Pseudo Code:
public class IndexOf {

   public static void main(String[] args) {
   
      ArrayList<String> al = new ArrayList<String>();
      
      al.add("AB");
      al.add("CD");
      al.add("EF");
      al.add("GH");
      al.add("IJ");
      al.add("KL");
      al.add("MN");

      System.out.println("Index of 'AB': "+al.indexOf("AB"));
      System.out.println("Index of 'KL': "+al.indexOf("KL"));
      System.out.println("Index of 'AA': "+al.indexOf("XX"));
      System.out.println("Index of 'AA': "+al.indexOf("YY"));
      System.out.println("Index of 'EF': "+al.indexOf("EF"));
      
   }
   
}
 
*/


import java.util.ArrayList;

public class IndexOf {
	
    public static void main(String[] args) {
	   
        ArrayList<String> al = new ArrayList<String>();
      
        al.add("AB");
        al.add("CD");
        al.add("EF");
        al.add("GH");
        al.add("IJ");
        al.add("KL");
        al.add("MN");

        System.out.println("Index of 'AB': "+al.indexOf("AB"));
        System.out.println("Index of 'KL': "+al.indexOf("KL"));
        System.out.println("Index of 'AA': "+al.indexOf("XX"));
        System.out.println("Index of 'AA': "+al.indexOf("YY"));
        System.out.println("Index of 'EF': "+al.indexOf("EF"));
      
    }
   
}


