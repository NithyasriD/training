/*
Requirement:
 - 6.Write a program to find the average age of all the Person in the person List

Entity:
 - AverageAge

Method signature:
 - public static void main(String[] args)

Jobs To be Done:
     1. Create a list of type person and add the list values to it. 
  	 2. Get the ages of the people and the collect it to list.
     3.Find the sum of the ages of the people
  	 4. Find the average of the age and print it.
  	 
PseudoCode:
 public class AverageAge {
	
	public static void main(String[] args) {
		
		CREATE list of type Person .
        ADD values to the list using list.add().
		FILTER and COLLECT the ages.
		for(int i=0 ;i< result.size() ;i++) {
			sum = sum + age;
		}
		avg = sum / result.size();
		System.out.println("Average age is:" +avg);
	}

 }
*/

package com.training.java.core.stream;

import java.util.List;
import java.util.stream.Collectors;

public class AverageAge {
	
	public static void main(String[] args) {
		
		List<Person> list = Person.createRoster();
		List<Person> result = list.stream().filter(x -> (x.getAge() > 0 ))
				                           .collect(Collectors.toList());
		
		int sum=0, avg;
		for(int i=0 ;i< result.size() ;i++) {
			sum = sum + result.get(i).getAge();
		}
		avg = sum / result.size();
		System.out.println("Average age is:" +avg);
		
	}

}
