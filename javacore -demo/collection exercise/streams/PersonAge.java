/*
 Requirement:1.To filter the Person, who are male and age greater than 21
  
 Entity:PersonAge
  
 Function Declaration : public static void main(String[] args);
  
 Jobs To be Done: 1. Create a list of type person and add the list values to it. 
  				  2. Get the ages of the people and filter the names whose age is greater than 21
  				  3. Store it into a stream.
  				  4. Print the age and names.
  				  
PseudoCode:
public class PersonAge {

	public static void main(String[] args) {
		
		List<Person> list = Person.createRoster();
		Find the names of the people whose age is greater than 21.
		stream.forEach(y -> System.out.println(y.getName()));
		
	}
	
}

 */
package com.training.java.core.stream;

import java.util.List;
import java.util.stream.Stream;

public class PersonAge {

	public static void main(String[] args) {
		
		List<Person> list = Person.createRoster();
		Stream<Person> stream = list.stream()
				                    .filter(x -> (x.getGender() == Person.Sex.MALE) && (x.getAge() > 21));
		
		stream.forEach(y -> System.out.println(y.getName()));
		
	}
	
}
