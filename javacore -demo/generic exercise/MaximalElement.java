package com.training.java.core.generic;
/* 
1.Requirement:
    Write a generic method to find the maximal element in the range [begin, end) of a list.
    
2.Entity:
    public class MaximalElement

3.Function Declaration:
    public static void main(String[] args)
    public static <T extends Object & Comparable<? super T>> T max(List<? extends T> list, int begin, int end)
    
4.Jobs to be Done:
    1. Create a class MaximalElement.
    2. Declare a method public static <T extends Object & Comparable<? super T>> T max(List<? extends T> list, int begin, int end)
    3. Compare the elements and find the maximum between the range and return it
    4. Under the main method create an object list for List<Integer>
    5. Invoking the method max() and print the result.

Pseudo Code:
public class MaximalElement {   
    public static <T extends Object & Comparable<? super T>> T max(List<? extends T> list, int begin, int end) {
        T maximumElement = list.get(begin);
        for(++begin; begin < end; ++begin) {
            if (maximumElement.compareTo(list.get(begin)) < 0) {
                maximumElement = list.get(begin);
            }
        }   
        return maximumElement;
    }
    
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8);
        System.out.println(max(list, 3, 6));
    }
}

*/

import java.util.Arrays;
import java.util.List;

public class MaximalElement { 
	
    public static <T extends Object & Comparable<? super T>> T max(List<? extends T> list, int begin, int end) {
    	
        T maximumElement = list.get(begin);
        for(++begin; begin < end; ++begin) {
            if (maximumElement.compareTo(list.get(begin)) < 0) {
                maximumElement = list.get(begin);
            }
        }   
        return maximumElement;
        
    }
    
    public static void main(String[] args) {
    	
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8);
        System.out.println(max(list, 3, 6));
        
    }
    
}
