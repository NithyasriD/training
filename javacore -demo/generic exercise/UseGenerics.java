package com.training.java.core.generic;
/*5. What will be the output of the following program?
public class UseGenerics {
    public static void main(String args[]){  
        MyGen<Integer> m = new MyGen<Integer>();  
        m.set("merit");
        System.out.println(m.get());
    }
}
class MyGen<T>
{
    T var;
    void  set(T var)
    {
        this.var = var;
    }
    T get()
    {
        return var;
    }
}

 1.Requirements : 
     - What will be the output of the following program?
     
 2.Entities :
 	 - UseGenerics.
 	 - MyGen<T>
 	 
 3.Function Declaration :
 	 - void  set(String var)
 	 
 4.Jobs To Be Done:
 		1.Creating the empty MyGen instance with type integer and set the value is merit
 		2.Creating a class MyGen 
 		3.In set() method store the String value.
 		4.In the given code to correct the syntax and call a get() method
 		5.Return the var.
 		
Pseudo Code:
public class UseGenerics {
    public static void main(String args[]){  
        MyGen<Integer> print = new MyGen<Integer>();  
        print.set("merit");
        System.out.println(print.get());
    }
}
class MyGen<T>
{
    T var;
    void  set(String var)
    {
        //this.var = var;
        this.var =(T) var;
    }
    T get()
    {
        return var;
    }
}


 */

public class UseGenerics {
    public static void main(String args[]){  
        MyGen<Integer> print = new MyGen<Integer>();  
        print.set("merit");
        System.out.println(print.get());
    }
}
class MyGen<T>
{
    T var;
    void  set(String var)
    {
        //this.var = var;
        this.var =(T) var;
    }
    T get()
    {
        return var;
    }
}

