package com.training.java.core.lambda;
/*
1.Requirements
   - Program to print difference of two numbers using lambda expression and the single method interface
   
2.Entities
   - DifferenceNumber
   - Different (Interface)
   
3.Function Declaration
   - int numbers(int number1,int number2);
   - public static void main(String[] args)
   
4.Jobs to be done
   1.Create a class as Different with interface as DifferenceNumbers
   2.Inside the declaring the single method with two integer parameters.
   3.In the class main creating interface object and assigning the lambda expression to return two integers difference.
   4.Print statement invoking the interface single method with integer values and finally return the value using assigned
interface object lambda expression

Pseudo Code:
interface Different {
	int numbers(int number1,int number2);
}

public class DifferenceNumber {
	public static void main(String[] args) {
		Different different = (number1,number2) -> number1 - number2;
		System.out.println("The Difference Number is : " + different.numbers(15,7));
	}
}
*/


interface Different {
	
	int numbers(int number1,int number2);

}

public class DifferenceNumber {
	
	public static void main(String[] args) {
		
		Different different = (number1,number2) -> number1 - number2;
		System.out.println("The Difference Number is : " + different.numbers(15,7));
		
	}
	
}
