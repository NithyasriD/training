 - What is serialization?
 
 Answer:
    *Serialization is the process of turning an object in memory into a stream of bytes 
so you can do stuff like store it on disk or send it over the network  or stored in a persistent storage.
    *Serialization provides a way where an object can be represented as a sequence of bytes which includes 
the object's data and information having the object's type and the types of data stored in the object.
	*It can only be read from the file after the serialized object is written into a file.	