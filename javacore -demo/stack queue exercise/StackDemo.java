package com.training.java.core.exercise;
/*  
1.Requirements :
  Create a stack using generic type and implement
  -> Push at least 5 elements
  -> Pop the peek element
  -> search a element in stack and print index value
  -> print the size of stack
  -> print the elements using Stream
  Reverse List Using Stack with minimum 7 elements in list.
2.Entities:
  - StackDemo 
3.Function Declaration:
  - public static void main(String[] args)
4.Job to be done :  
   1.Create a class name StackDemo and declare main method
   2.In the main creating the Stack with generic String type and creating stream for displaying the Stack elements.
   3.Pushing 5 values to stack using push() method and using pop() method displaying top value in stack.
   4.Search the specified value using search() method and find size of stack using size() method.
   5.To print the index value in stack using specifying index.
   6.Printing the stack values using stream and for each.
   7.To reverse a stack creating another stack removing the values using remove() method and pushing to new created 
stack using push() method 
   8.Removing elements from stack adding to list using add() method.
   9.Print the softwares.
 
Pseudo Code:
public class StackDemo {
    public static void main(String[] args) {
        stack<String> software = new Stack<String>();
        
        //pushing values
        softwares.push("JAVA");
        softwares.push("Python");
        softwares.push("R Language");
        softwares.push("Ruby"); 
        softwares.add("C Language");  
        
        //pop top element in stack
        softwares.pop();  
        
        //search the element
        int index = softwares.search("Ruby");  
        int size = softwares.size(); 
        System.out.println(index);      
        stream.forEach((element) -> {
           System.out.println(element);  
        });
        
        Stack<String> stack = new Stack<String>();
        while(softwares.size() > 0) {
                  stack.push(softwares.remove(0)); 
        }
        
        while(stack.size() > 0){
                softwares.add(stack.pop()); 
        } 
        System.out.println(softwares);      
    }    
} 
*/

import java.util.Stack;
import java.util.stream.Stream;

public class StackDemo {
	
    public static void main(String [] args) {
    	
        Stack<String> softwares = new Stack<String>();  //Creating Stack using generic type
        Stream<String> stream = softwares.stream();
        
        softwares.push("JAVA");
        softwares.push("Python");
        softwares.push("R Language");
        softwares.push("Ruby"); 
        softwares.add("C Language");  
        
        softwares.pop();   //pop top element in stack
        System.out.println("-----Create a stack using generic type and implement-----");
        
        int index = softwares.search("Ruby");  
        int size = softwares.size();        
        System.out.println(size);       
        System.out.println(index);      
        stream.forEach((element) -> {
           System.out.println(element);  
        });    
        
        System.out.println("-----Reverse List Using Stack with minimum 7 elements in list-----");
        Stack<String> stack = new Stack<String>();
        while(softwares.size() > 0) {
                  stack.push(softwares.remove(0)); 
        }

        while(stack.size() > 0){
                softwares.add(stack.pop()); 
        } 

        System.out.println(softwares);
        
    }
    
}
