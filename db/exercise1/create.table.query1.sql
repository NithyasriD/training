create schema database2;

create table employee(
 emp_id int 
,emp_name varchar(40)
,dob date
,salary int);

alter table employee 
rename to employee_detail;

create table department(
 dep_id int
,dep_name varchar(40));

create table order_list(
 ord_no int
,ord_name varchar(40));

drop table order_list;