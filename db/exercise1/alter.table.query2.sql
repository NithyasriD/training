alter table employee_detail 
  add date_of_joining date;
alter table employee_detail 
  add email varchar(40);
 
alter table employee_detail 
 rename column dob 
   to date_of_birth;

alter table employee_detail 
modify column email varchar(100);

alter table employee_detail
 drop column email;