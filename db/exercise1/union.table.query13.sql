alter table employee_detail 
 add column area varchar(40) not null after dep_id;

update employee_detail set area ='coimbatore' where emp_id = '6';
update employee_detail set area ='tiruppur' where emp_id = '10';
update employee_detail set area ='erode' where emp_id = '91';
update employee_detail set area ='chennai' where emp_id = '93';
update employee_detail set area ='trichy' where emp_id = '96';

alter table department 
 add column area varchar(40) not null after dep_id;

update  department set area = 'erode' where dep_id = '2';
update  department set area = 'coimbatore' where dep_id = '3';
update  department set area = 'trichy' where dep_id = '1';
update  department set area = 'tiruppur' where dep_id = '5';
update  department set area = 'chennai' where dep_id = '4';

select emp_id
      ,emp_name
      ,date_of_birth
      ,salary
      ,date_of_joining
      ,dep_id
      ,area 
 from employee_detail;
select dep_id
      ,area
      ,dep_name
  from department;



SELECT dep_id
     , area 
  FROM employee_detail
 WHERE area = 'coimbatore'
 UNION ALL
SELECT dep_id
     , area 
  FROM department
 WHERE area='coimbatore'
ORDER BY dep_id;



SELECT DISTINCT emp_name
			   ,dep_id
  FROM employee_detail;



SELECT ALL emp_id
          ,emp_name
          ,dep_id
          ,salary
          ,area
      FROM employee_detail;

