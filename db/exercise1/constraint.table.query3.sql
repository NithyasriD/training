ALTER TABLE employee_detail
  MODIFY emp_id int NOT NULL;
ALTER TABLE employee_detail
  MODIFY emp_name int NOT NULL;
ALTER TABLE department
  MODIFY dep_id int NOT NULL;  
  
ALTER TABLE employee_detail
  ADD UNIQUE (emp_id);
ALTER TABLE employee_detail
  ADD UNIQUE (dep_id);
  
  
ALTER TABLE employee_detail
  ADD PRIMARY KEY (emp_id);
ALTER TABLE department
  ADD PRIMARY KEY (dep_id);  
  
ALTER TABLE employee_detail 
  ADD COLUMN dep_id int;
  
ALTER TABLE employee_detail
  ADD FOREIGN KEY (dep_id)
  REFERENCES department(dep_id); 
  
ALTER TABLE employee_detail  
  ADD CHECK (salaryTABLE >=100000);
ALTER TABLE employee_detail
  ADD CONSTRAINT CHK_EmpSalary CHECK (salary >= 100000);  
ALTER TABLE employee_detail
 DROP CONSTRAINT CHK_EmpSalary;

  
ALTER TABLE employee_detail
ALTER emp_name SET DEFAULT 'nithya';  
  
CREATE INDEX idx_name
    ON employee_detail (emp_name);
  
  