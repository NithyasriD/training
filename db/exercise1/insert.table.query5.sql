insert into department values('1','ITDesk');
insert into department values('2','Finance');
insert into department values('3','Engineering');
insert into department values('4','HR');
insert into department values('5','Recruitment');

select dep_id
      ,area
      ,dep_name 
  from department;




insert into employee_detail values('091','nithya','2001-02-15','500000','2020-08-01','1');
insert into employee_detail values('092','sri','2000-01-09','450000','2020-08-02','2');
insert into employee_detail values('093','nathiya','2001-01-22','510000','2020-08-03','3');
insert into employee_detail values('094','sriram','2000-09-20','530000','2020-08-04','5');
insert into employee_detail values('095','kumar','2001-10-25','550000','2020-08-05','4');
insert into employee_detail values('096','roja','2000-11-24','450000','2020-08-06','4');

select emp_id
      ,emp_name
      ,date_of_birth
      ,salary
      ,date_of_joining
      ,dep_id
      ,area 
  from employee_detail;