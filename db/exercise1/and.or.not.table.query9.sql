SELECT emp_id
      ,emp_name
      ,date_of_birth
      ,salary
      ,date_of_joining
      ,dep_id
      ,area 
  FROM employee_detail
 WHERE emp_name = 'nithya' 
   AND dep_id = '1';



SELECT emp_id
      ,emp_name
      ,date_of_birth
      ,salary
      ,date_of_joining
      ,dep_id
      ,area
  FROM employee_detail
 WHERE emp_name = 'nithya' 
    OR emp_name = 'nathiya';        




SELECT emp_id
      ,emp_name
      ,date_of_birth
      ,salary
      ,date_of_joining
      ,dep_id
      ,area
  FROM employee_detail
 WHERE NOT emp_name='nithya';




SELECT emp_id
      ,emp_name
      ,date_of_birth
      ,salary
      ,date_of_joining
      ,dep_id
      ,area
  FROM employee_detail
 WHERE emp_name LIKE '%a'; 
SELECT emp_id
      ,emp_name
      ,date_of_birth
      ,salary
      ,date_of_joining
      ,dep_id
      ,area
  FROM employee_detail
 WHERE emp_name NOT LIKE 'a%';
SELECT emp_id
      ,emp_name
      ,date_of_birth
      ,salary
      ,date_of_joining
      ,dep_id
      ,area
  FROM employee_detail
 WHERE emp_name LIKE 'n%';




SELECT emp_id
      ,emp_name
      ,date_of_birth
      ,salary
      ,date_of_joining
      ,dep_id
      ,area
  FROM employee_detail
 WHERE emp_name 
    IN ('nithya', 'nathiya', 'roja');
SELECT emp_id
      ,emp_name
      ,date_of_birth
      ,salary
      ,date_of_joining
      ,dep_id
      ,area
  FROM employee_detail
 WHERE emp_name 
   NOT IN ('nithya', 'nathiya', 'roja');




SELECT emp_name
      ,salary
      ,emp_id
  FROM employee_detail
 WHERE dep_id = ANY (
 SELECT dep_id 
   FROM department 
  WHERE dep_name = 'Finance'); 




SELECT emp_id
      ,emp_name
      ,date_of_birth
      ,salary
      ,date_of_joining
      ,dep_id
      ,area 
  FROM employee_detail
 WHERE emp_name 
 LIKE 'nak%';
SELECT emp_id
      ,emp_name
      ,date_of_birth
      ,salary
      ,date_of_joining
      ,dep_id
      ,area
  FROM employee_detail
 WHERE emp_name 
 LIKE 'r_ja';




        