SELECT employee_detail.emp_id
     , employee_detail.emp_name
     , department.dep_id
     , department.dep_name
  FROM employee_detail
 INNER JOIN department 
	ON employee_detail.dep_id = department.dep_id;
	
	

SELECT employee_detail.emp_id
	  ,employee_detail.emp_name
      ,employee_detail.date_of_birth
      ,employee_detail.salary
      ,employee_detail.date_of_joining
      ,employee_detail.dep_id
      ,employee_detail.area
      ,department.dep_id
      ,department.area
      ,department.dep_name
  FROM employee_detail
  LEFT JOIN department
    ON (employee_detail.emp_id = department.dep_id);



SELECT employee_detail.emp_id
	  ,employee_detail.emp_name
      ,employee_detail.date_of_birth
      ,employee_detail.salary
      ,employee_detail.date_of_joining
      ,employee_detail.dep_id
      ,employee_detail.area
      ,department.dep_id
      ,department.area
      ,department.dep_name
  FROM employee_detail
 RIGHT JOIN department
    ON (employee_detail.emp_id = department.dep_id);



SELECT employee_detail.emp_id
      ,employee_detail.emp_name
      ,employee_detail.date_of_birth
      ,employee_detail.salary
      ,employee_detail.date_of_joining
      ,employee_detail.dep_id
      ,employee_detail.area
      ,department.dep_id
      ,department.area
      ,department.dep_name 
  FROM employee_detail 
 CROSS JOIN department;
 
 
 