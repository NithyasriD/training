UPDATE employee 
   SET annual_salary = annual_salary + (annual_salary * 10 / 100) 
 WHERE dep_id ;
 
 
SELECT emp_id
      ,first_name
      ,surname
      ,dob
      ,annual_salary
      ,date_of_joining
      ,dep_id
      ,area 
  FROM employee
 WHERE annual_salary
   IN (SELECT max(annual_salary)
  FROM employee
 WHERE dep_id);
 
 
SELECT emp_id
      ,first_name
      ,surname
      ,dob
      ,annual_salary
      ,date_of_joining
      ,dep_id
      ,area 
  FROM employee
 WHERE annual_salary
   IN (SELECT min(annual_salary)
  FROM employee
 WHERE dep_id);   
  
  
SELECT AVG(annual_salary) 
  FROM employee;     

