alter table employee
 drop foreign key dep_id;

alter table employee
change column dep_id dep_id int null;

alter table employee
  add constraint dep_id
  foreign key(dep_id)
  references department(dep_id);
 
select emp_id
      ,first_name 
      ,surname
      ,dob
      ,date_of_joining
      ,annual_salary
      ,area 
  from employee
 where dep_id is null;