alter table employee
  add column area varchar(50) not null after dep_id;

update employee set area = 'tiruppur' where emp_id = '100';
update employee set area = 'coimbatore' where emp_id = '101';
update employee set area = 'erode' where emp_id = '102';
update employee set area = 'chennai' where emp_id = '103';
update employee set area = 'erode' where emp_id = '104';
update employee set area = 'trichy' where emp_id = '105';
update employee set area = 'tiruppur' where emp_id = '106';
update employee set area = 'trichy' where emp_id = '107';
update employee set area = 'coimbatore' where emp_id = '108';
update employee set area = 'erode' where emp_id = '109';
update employee set area = 'erode' where emp_id = '110';
update employee set area = 'trichy' where emp_id = '111';
update employee set area = 'chennai' where emp_id = '112';
update employee set area = 'chennai' where emp_id = '113';
update employee set area = 'tiruppur' where emp_id = '114';
update employee set area = 'chennai' where emp_id = '115';
update employee set area = 'erode' where emp_id = '116';
update employee set area = 'coimbatore' where emp_id = '117';
update employee set area = 'trichy' where emp_id = '118';
update employee set area = 'coimbatore' where emp_id = '119';
update employee set area = 'tiruppur' where emp_id = '120';
update employee set area = 'trichy' where emp_id = '121';
update employee set area = 'coimbatore' where emp_id = '122';
update employee set area = 'chennai' where emp_id = '123';
update employee set area = 'tiruppur' where emp_id = '124';

select dep_id
      ,first_name
      ,surname
      ,area 
  from employee 
 where area = 'tiruppur' 
   and dep_id = '1'; 

select dep_id
      ,first_name
      ,surname
      ,area 
  from employee 
 where area = 'coimbatore' 
   and dep_id = '2'; 

select dep_id
      ,first_name
      ,surname
      ,area 
  from employee
 where area = 'trichy' 
   and dep_id = '3'; 

select dep_id
      ,first_name
      ,surname
      ,area 
  from employee 
 where area = 'chennai'
   and dep_id = '4'; 

select dep_id
      ,first_name
      ,surname
      ,area 
  from employee 
 where area = 'erode' 
   and dep_id = '5'; 


