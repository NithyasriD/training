create schema database1;

create table employee(
 emp_id int not null
,first_name varchar(40) not null
,surname varchar(40) 
,dob date 
,date_of_joining date
,annual_salary int 
,primary key(emp_id));

create table department(
 dep_id int not null
,dep_name varchar(40) not null
,primary key(dep_id));

insert into employee values('100','nithya','D','2001-02-15','2020-08-01','400000');
insert into employee values('101','sri','M','2000-02-15','2020-08-02','300000');
insert into employee values('102','nathiya','D.M','2007-10-25','2020-08-03','300000');
insert into employee values('103','sriram','M','2000-11-23','2020-08-04','280000');
insert into employee values('104','kumar','K','2000-12-21','2020-08-05','190000');
insert into employee values('105','roja','K','2001-10-20','2020-07-03','290000');
insert into employee values('106','kowsika','A','2000-08-21','2020-07-04','300000');
insert into employee values('107','visa','B','2000-12-15','2020-07-05','310000');
insert into employee values('108','jeevi','C','1999-09-25','2020-07-06','320000');
insert into employee values('109','pavi','D','1999-10-24','2020-07-07','330000');
insert into employee values('110','abi','E','1998-10-15','2020-07-08','340000');
insert into employee values('111','gopika','F','1999-10-19','2020-07-09','350000');
insert into employee values('112','hema','G','2000-01-15','2020-07-10','300000');
insert into employee values('113','celciya','H','2000-03-22','2020-07-11','400000');
insert into employee values('114','machu','I','2000-02-26','2020-07-12','380000');
insert into employee values('115','kavi','J','2001-04-15','2020-07-13','300000');
insert into employee values('116','priya','K','2001-11-20','2020-07-14','320000');
insert into employee values('117','kani','L','2000-05-26','2020-07-15','330000');
insert into employee values('118','karthika','M','1998-03-22','2020-07-16','340000');
insert into employee values('119','deepa','N','2000-11-25','2020-07-18','350000');
insert into employee values('120','joshika','O','2001-10-20','2020-06-19','350000');
insert into employee values('121','bhavana','P','2000-11-13','2020-06-20','310000');
insert into employee values('122','divya','R','1999-01-12','2020-05-23','200000');
insert into employee values('123','deepika','S','1998-02-02','2020-04-24','290000');
insert into employee values('124','poorni','T','1997-03-15','2020-02-25','370000');

select emp_id
      ,first_name
      ,surname
      ,dob
      ,date_of_joining
      ,annual_salary
      ,dep_id
      ,area
  from employee;

insert into department values('1','ITDesk');
insert into department values('2','Finance');
insert into department values('3','Engineering');
insert into department values('4','HR');
insert into department values('5','Recruitment');

select dep_id
      ,dep_name
  from department;

ALTER TABLE employee 
  ADD COLUMN dep_id INT not null;

ALTER TABLE employee
  ADD FOREIGN KEY (dep_id)
REFERENCES department(dep_id);

update employee set dep_id = 1 where emp_id = 100; 
update employee set dep_id = 2 where emp_id = 101; 
update employee set dep_id = 3 where emp_id = 102; 
update employee set dep_id = 4 where emp_id = 103; 
update employee set dep_id = 5 where emp_id = 104; 
update employee set dep_id = 1 where emp_id = 105; 
update employee set dep_id = 3 where emp_id = 106; 
update employee set dep_id = 5 where emp_id = 107; 
update employee set dep_id = 4 where emp_id = 108; 
update employee set dep_id = 2 where emp_id = 109; 
update employee set dep_id = 5 where emp_id = 110; 
update employee set dep_id = 4 where emp_id = 111; 
update employee set dep_id = 3 where emp_id = 112; 
update employee set dep_id = 2 where emp_id = 113; 
update employee set dep_id = 1 where emp_id = 114;
update employee set dep_id = 1 where emp_id = 115; 
update employee set dep_id = 2 where emp_id = 116; 
update employee set dep_id = 3 where emp_id = 117; 
update employee set dep_id = 4 where emp_id = 118; 
update employee set dep_id = 5 where emp_id = 119; 
update employee set dep_id = 4 where emp_id = 120; 
update employee set dep_id = 3 where emp_id = 121; 
update employee set dep_id = 5 where emp_id = 122;
update employee set dep_id = 2 where emp_id = 123; 
update employee set dep_id = 1 where emp_id = 124; 

select emp_id
      ,first_name
      ,surname 
      ,dob
      ,date_of_joining
      ,annual_salary
      ,dep_id
      ,area  
  from employee; 

 
