ALTER TABLE `employee` 
CHANGE COLUMN `emp_name` `emp_name` VARCHAR(100) NULL ,
CHANGE COLUMN `emp_dob` `emp_dob` DATE NULL ,
CHANGE COLUMN `email` `email` VARCHAR(50) NULL ,
CHANGE COLUMN `phone` `phone` BIGINT NULL ;

INSERT INTO `employee` (`employee_id`
	   ,`college1_id`
	   ,`cdept0_id`
	   ,`designation_id`) 
VALUES ('105'
       ,'94'
       ,'20'
       ,'2');
INSERT INTO `employee` (`employee_id`
	   ,`college1_id`
	   ,`cdept0_id`
	   ,`designation_id`) 
VALUES ('106'
       ,'96'
       ,'40'
       ,'4');

SELECT designation.desig_name 
      ,designation.rank
      ,college.college_name 
      ,department.dept_name
      ,university.university_name
  FROM employee
      ,college_department
      ,department
      ,university 
      ,designation 
      ,college
 WHERE employee.emp_name is NULL
   AND college.univ_code = university.univ_code 
   AND department.univ_code1 = university.univ_code
   AND college_department.college_id = college.college_id 
   AND college_department.dept_code = department.dept_code
   AND employee.college1_id = college.college_id
   AND employee.cdept0_id = college_department.cdept_id 
   AND employee.designation_id = designation.designation_id
