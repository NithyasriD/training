select roll_number
       ,student_name
       ,gender
       ,student_dob
       ,s.email
       ,s.phone
       ,s.address
       ,college_name
       ,dept_code as dept_name
       ,university_name
       ,employee_name
       ,designation_name 
   from student s
  inner join college_department
     as cd 
	 on cd.cdept_id = s.fk2_cdept_id    
  inner join college 
     as c 
	 on c.college_id = cd.fk_college_id  
  inner join university_department 
     as ud 
	 on ud.univdept_id = cd.fk_univdept_id
  inner join university
     as u
	 on u.univ_id = c.fk_univ_id
  inner join employee
     as e 
	 on e.fk_cdept_id = s.fk2_cdept_id
  inner join designation
     as dg
	 on dg.designation_id = e.fk_designation_id
  where designation_id = '3';


