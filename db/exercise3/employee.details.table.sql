select employee_name
      ,employee_id
      ,phone
      ,college_name
      ,dept_code
      ,designation_name
      ,d.rank
  from employee emp
 inner join college_department 
    as cd 
	on cd.cdept_id = emp.fk_cdept_id 
 inner join college 
    as c 
	on c.college_id = cd.fk_college_id
 inner join university_department
    as ud
	on ud.univdept_id = cd.fk_univdept_id
 inner join designation 
    as d 
	on d.designation_id = emp.fk_designation_id
 order by d.rank
      ,college_name;