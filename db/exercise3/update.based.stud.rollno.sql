UPDATE semester_fee 
   SET paid_year = 2020 
      ,paid_status = "paid" 
 WHERE "st002" = (
SELECT roll_number
  FROM student
 WHERE semester_fee.fk_stud_id = student.student_id);


UPDATE semester_fee 
   SET paid_year = 2019
      ,paid_status = "Paid" 
 WHERE (
SELECT roll_number
  FROM student 
 WHERE semester_fee.fk_stud_id = student.student_id) 
    in ("st010" , "st013", "st018");
    
    
SELECT semester_fee_id
      ,fk_stud_id
	  ,semester
	  ,amount
	  ,paid_date
	  ,paid_status
	  ,paid_year
  FROM database4.semester_fee;

    
    