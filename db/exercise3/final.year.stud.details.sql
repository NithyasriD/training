select roll_number
       ,student_name
       ,gender
       ,student_dob
       ,email
       ,phone
       ,address
       ,college_name
       ,dept_code
       ,university_name
   from student s
  inner join college_department
     as cd 
	 on cd.cdept_id = s.fk2_cdept_id    
  inner join college
     as c 
	 on c.college_id = cd.fk_college_id  
  inner join university_department 
     as ud 
	 on ud.univdept_id = cd.fk_univdept_id
  inner join university
     as u 
	 on u.univ_id = c.fk_univ_id
  where s.academic_year = '2016'
    and city = 'coimbatore';



