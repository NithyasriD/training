ALTER TABLE `semester_fee` 
  ADD COLUMN `balance_amount` DOUBLE NOT NULL 
AFTER `paid_status`;

SELECT university.`university_name`
      ,college.`college_name`
      ,semester_fee.`semester`
      ,SUM(amount) AS 'collected_fees' 
      ,semester_fee.paid_year
  FROM semester_fee
      ,university
      ,student
      ,college
 WHERE semester_fee.stud_id = student.student_id
   AND student.college2_id = college.college_id
   AND college.univ_code = university.univ_code
   AND university_name = 'delhi university'
   AND paid_year = '2020'
   AND semester = '5'
   AND paid_status = 'paid';


SELECT university.`university_name`
      ,college.`college_name`
      ,semester_fee.`semester`
      ,SUM(balance_amount) AS 'uncollected_fees' 
  FROM semester_fee
      ,university
      ,student
      ,college
 WHERE semester_fee.stud_id = student.student_id
   AND student.college2_id = college.college_id
   AND college.univ_code = university.univ_code
   AND university_name = 'anna university'
   AND semester = '5';


SELECT university.`university_name`
      ,SUM(amount) AS 'collected_fees' 
      ,semester_fee.paid_year
  FROM semester_fee
      ,university
      ,student
      ,college
 WHERE semester_fee.stud_id = student.student_id
   AND student.college2_id = college.college_id
   AND college.univ_code = university.univ_code
   AND university_name = 'mumbai university'
   AND paid_status = 'paid'
   AND paid_year = '2020'
