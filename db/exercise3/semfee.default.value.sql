create table sem_fee(
  sem_fee_id int
 ,semester int
 ,amount int
 ,paid_year year
 ,fk3_stud_id int
 ,paid_status varchar(6));
 
insert into sem_fee values('1','4','40000','2020','1','unpaid'); 


alter table semfee 
  add column amount int;
  
alter table sem_fee 
alter amount 
  set default '55000'; 


alter table semfee modify column paid_year varchar(5);
alter table sem_fee 
alter paid_year
  set default 'NULL'; 


alter table semfee 
  add column paid_status varchar(6);
  
alter table sem_fee
alter paid_status 
  set default 'Unpaid'; 

insert into sem_fee(sem_fee_id,semester,fk3_stud_id) values('2','5','2');


select fk3_stud_id
      ,semester 
	  ,sf.amount
      ,sf.paid_year
      ,sf.paid_status
  from sem_fee sf;
  
  
  
 