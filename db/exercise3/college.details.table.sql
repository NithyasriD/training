
select univ_code
      ,university_name
      ,code
      ,college_name
      ,city
      ,state
      ,year_opened
      ,dept_code as department_name 
      ,employee_name as NAME
      ,designation_name
  from university u 
 inner join college 
    on u.univ_id = college.fk_univ_id
 inner join university_department 
    as ud 
	on u.univ_id = ud.fk1_univ_id
 inner join employee 
    as emp
	on emp.fk2_college_id = college.college_id
 inner join designation 
    as de 
	on de.designation_id = emp.fk_designation_id
 where dept_code in('CSE','IT')
   and designation_id = '3';