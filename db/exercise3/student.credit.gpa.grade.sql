select student_id
      ,roll_number
      ,student_name
      ,student_dob
      ,gender
      ,phone 
      ,grade
      ,credits
      ,gpa
      ,semester
      ,num_semester
      ,college_name
      ,university_name
  from student s
 inner join semester_result 
    as sr 
	on sr.fk1_stud_id = s.student_id
 inner join college_department 
    as cd 
	on cd.cdept_id = s.fk2_cdept_id
 inner join college 
    as c 
    on c.college_id = cd.fk_college_id 
 inner join university  
    as u 
	on u.univ_id = c.fk_univ_id
 order by sr.semester,college_name;