SELECT university_name
      ,SUM(amount) AS 'collected_fees' 
      ,semester_fee.paid_year
  FROM semester_fee
      ,university
      ,student
      ,college
 WHERE semester_fee.fk_stud_id = student.student_id
   AND college.fk_univ_id = university.univ_id
   AND university_name = 'anna university'
   AND paid_status = 'paid'
   AND paid_year = '2019';


